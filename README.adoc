= Groogle documentation
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle Drive DSL
:keyworks: groovy, google, groogle, dsl, drive
:hide-uri-scheme: 

This is the Groogle documentation repository. 

This documentation links to subsequent documentation in the subprojects. You can review them at https://gitlab.com/groogle[Groogle group projects]

Visit https://groogle.gitlab.io

Start from `src/docs/antora/modules/ROOT/pages/index.adoc`