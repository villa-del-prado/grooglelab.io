= Groogle
:icons: font
:source-highlighter: highlightjs
:revealjs_theme: solarized
:revealjs_transition': linear


Groovy + Google = Groogle

== Presentación

Jorge Aguilera

@jagedn

jorge.aguilera@puravida-software.com

[transition=zoom]
== Qué vamos a ver

[%step]
* DSL
* Groogle
* Demo time

== DSL

[quote,Martin Fowler,https://martinfowler.com/books/dsl.html]
DSLs are small languages, focused on a particular aspect of a software system.
You can't build a whole program with a DSL, but you often use multiple DSLs in a
system mainly written in a general purpose language.

=== DSL "famosos"

[source,sql]
----
SELECT * FROM TABLE WHERE FIELD1 = 'STR' AND FIELD2 > 20
----

=== HTML


[source,html]
----
<html>
    <body>
        <div>
            <span>Hola</span>
        </div>
    </body>
</html>
----

=== Closure

.Gradle (Closures)
[source,groovy]
----
repositories {
    mavenCentral()
    jcenter()
    maven {
        url  "https://dl.bintray.com/puravida-software/repo"
    }
}
----

=== Fluent

.Spring Integration (Builder)
[source,java]
----
IntegrationFlows.from("example")
    .channel(this.inputChannel())
    .filter((Integer p) -> p > 0)
    .transform(Object::toString)
    .channel(MessageChannels.queue())
    .get();
----


== Groogle

Groogle es un proyecto abierto que ofrece un
DSL para acceder a servicios de Google

https://groogle.gitlab.io/


== Google API's

Conjunto de API's que permiten el acceso a servicios de Google
(Gmail, Drive, Sheet, y un largo etcetera)

Casi todas son REST (pero complejas) así que hay librerías Java, Python, etc

https://developers.google.com/api-client-library/java/

== Librería y DSL

- Groogle comenzó como librería para autentificación pero ha evolucionado a
ofrecer DSL específicas en cada servicio, aunque pueden ser usadas en conjunto.

- Uso directo en GroovyScripts

- Groogle está implementado en Groovy pero se puede usar desde
Java 8 mediante _lambdaS_ (Consumer)

- Descargar como dependencias desde jCenter

== Casos de Uso

- Todo lo que puedes hacer con el API
- Volcar una tabla a un Sheet compartido (o viceversa)
- Envío de correos según Sheet y adjuntando ficheros Drive
- Gestionar eventos de un calendario de forma programada
- etc

== Lenguajes/Entornos

- Groovy script

- Java ( min 8)

- Grails, Ratpack

- Dockerizado

== Subproyectos

- Autentificación (OAuth y de servicio) [x]
- Drive [x]
- Sheet [x]
- Gmail [X]
- Calendar [x] (old)

- Team [ ]
- siguiente según demanda [ ]

== Auth

*groogle-core*

WARNING: Crear una aplicación en la consola de Google junto con unas credenciales.
Necesitamos descargar el _json_ y poder acceder a él (como resource, file, etc)

=== Formas de identificarse

- Autentificación OAuth2: el usuario selecciona la cuenta con la que trabajar

- Cuenta de Servicio. Diferentes usuarios permiten acceder a alguno de sus recursos

=== Tips

Definir los roles que vamos a usar (Drive, Calendar, Sheet ...)

Se crea una carpeta $HOME/.credentials/_${appname}_ donde guardar los tokens

=== API

.Authenticate.java
[source,java]
----
public static void main(String[] args) {
  .......
  DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
  final Credential credential = authorize();
  HttpRequestFactory requestFactory =
      HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
        @Override
        public void initialize(HttpRequest request) throws IOException {
          credential.initialize(request);
          request.setParser(new JsonObjectParser(JSON_FACTORY));
        }
      });
  run(requestFactory);
  .......
}
----

=== DSL

NOTE: Se abre una ventana en un navegador y seleccionamos cuenta de usuario

[source,groovy]
----
groogle = GroogleBuilder.build {
    credentials {
        applicationName 'test-gmail'
        withScopes DriveScopes.DRIVE
        usingCredentials "client_secret.json"
        asService false
    }
    service(DriveServiceBuilder.build(), DriveService)
}
----


== Drive

[source,groovy]
----
DriveService drive = groogle.service(DriveService)
int count = drive.findFiles {
    nameStartWith 'examp'
    batchSize 30
    eachFile {
        println file.id
    }
}
----

== Sheet

[source,groovy]
----
sheetService.createSpreadSheet "test", {
    createSheet "test", {
        writeRange 'A1', 'C3',{
            clear()
            set( [
                    ['Hola','=A1', 22],
                    ['=A1', '', '=C1'],
            ])
        }
    }
}
----

== Gradle Plugin

https://puravida-gradle.gitlab.io/groogle/


== Demo

Demo Time

== Links

barcode::qrcode[https://groogle.gitlab.io/groogle,300,300]

- https://groogle.gitlab.io/

- https://puravida-software.gitlab.io/


[%notitle]
== Gracias

image::gracias_multilingue.jpg[background, size=cover]
