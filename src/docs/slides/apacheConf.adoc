= Groogle: when Google met Groovy +
... or why to use Groovy in a DSL
:revealjsdir: https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.9.2
:icons: font
:source-highlighter: highlightjs
:revealjs_theme: solarized
:revealjs_transition': linear


== Presentation

Hi!

I'm Jorge Aguilera,

@pvidasoftware (PuraVida Software)

+-10 years programming in Groovy ... and learning every day a new feature

[%step]
== Know your audience

- raise your hand if you work/know Groovy?
- raise your hand if you work/know Google API?
- raise your hand if you know what a DSL is?

== What's a DSL?

[quote,Martin Fowler,https://martinfowler.com/books/dsl.html]
DSLs are *small* languages, focused on a *particular aspect* of a software system.
You can't build a whole program with a DSL, but you often use *multiple DSLs* in a
system mainly written in a general purpose language.

=== SQL

The (maybe) most famous DSL

[source]
----
select title, author, pages from books
----

=== Gradle

[source]
----
repositories {
    mavenCentral()
}
dependencies{
    api 'org.log4j:log4j:1.2.3'
}
----

=== Fluent

[source,java]
----
IntegrationFlows.from("example")
    .channel(this.inputChannel())
    .filter((Integer p) -> p > 0)
    .transform(Object::toString)
    .channel(MessageChannels.queue())
    .get();
----

== Groogle, the origin

Google + Groovy = Groogle

- started as a (lot of) Groovy Scripts for Google Calendar for a friend of mine
- a collection of DSL for Google APIs
- (the idea) simple to use for non-programmer users
- spoiler: my friend never used it

== Groogle

* Open Source, https://gitlab.com/groogle
* Still in Java 8-11
* Groovy 3.0.10
* Maven Central (com.puravida-software.groogle)
* Drive, Sheet, Gmail, Calendar, People
* BigQuery ("in progress")
* Photos, started
* ...

[.columns]
== Google vs Groogle

[.column.is-two-fifths]
image:google.png[]


[.column.is-two-fifths]
image:1.png[]

== Documentation

- Documented ... but in spanish ... by the moment

https://groogle.gitlab.io/groogle/latest/index.html

WARNING:: Probably the most difficult part of a DSL is the documentation


== Live coding

A live coding session is better than 1000 slides

( 🤞🤞🤞 )


== Why Groovy?

[quote,Apache Groovy,https://groovy-lang.org/] 
Is a powerful, optionally typed and dynamic language, with *static-typing* and *static compilation* capabilities, 
for the *Java platform* aimed at improving developer productivity thanks to a concise, familiar and easy to learn syntax. 

== Groovy DSL nature

- Command chains
- Operator overloading
- Enhancing classes
- Missing method/property
- @DelegatesTo

=== Command chains

.Java
[source]
----
please(show).the(square_root).of(100)
paint( wall).with( List.of(red, green)).and(yellow)
----

.Groovy
[source]
----
please show the square_root of 100
paint wall with red, green and yellow
----

=== Operator overloading

`Various operators in Groovy are mapped onto regular method calls on objects.`

So you can overwrite these methods, i.e:

[source]
----
def tobacco = new Tobaco()
def tomatoes = new Tomatoes()

def tomacco = tobacco + tomatoes  //<1>

Tobaco.plus( object ){
    return new Tomacco(nicotine:90)
}
----
<1> (TM) Homer Simpson


=== Enhancing classes

- extension and categories

[source]
----
use(TimeCategory)  {
    println 1.minute.from.now       
    println 10.hours.ago

    def someDate = new Date()       
    println someDate - 3.months
}
----

Categories are *lexically bound*.

=== Missing method/property

(do you remember A1='Title' example?)

[source]
----
def methodMissing(String name, def args) {
    return "this is me"
}

def propertyMissing(String name, value) {
    storage[name] = value
}

def propertyMissing(String name) {
    storage[name]
}
----


=== Closures

- Similar to Java lambda, it's an anonymous block of code
- Can receive arguments, return values, and be assigned
- Delegation: owner, delegate and this
- Delegation Strategy: OWNER_FIRST, DELEGATE_FIRST, OWNER_ONLY, DELEGATE_ONLY

=== @DelegatesTo

With @DelegatesTo we can instruct to the compiler which class
will execute the Closure

- static compile
- documentation
- IDE completion

[.columns]
=== @DelegatesTo

[.column.is-one-third]
image:email-dsl.png[]

[.column.is-one-third]
image:email.png[]

[.column.is-one-third]
image:email-spec.png[]

== Recap

- DSL can improve your API/Product
- Groovy has a lot of feature to help you
- @DelegatesTo as an assistant completion
- Have fun and learn from everything

Ok, nice, but who care a DSL ...

[.columns]
== Real use case: Nextflow DSL.

[.column.is-thow-fifths]
- nextflow.io
- _Nextflow enables scalable and reproducible scientific workflows using software containers._
- Write (a pipeline) once and run wherever (local, k8s, AWS, Google, Azure,...)

[.column.is-thow-fifths]
image:nextflow.png[]

[.columns]
=== Nextflow

[.column.is-two-fifths]
- DSL to orchestrate workflows
- Open Source, community driven
- Groovy 3.0.10 (4.x coming soon)
- GPars and others

[.column.is-two-fifths]
image::nextflow-stats.png[]

[%notitle]
== Thank you

image::gracias_multilingue.jpg[background, size=cover]

== Questions ?

