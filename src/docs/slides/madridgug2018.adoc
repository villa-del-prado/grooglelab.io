= Groogle
:icons: font
:source-highlighter: highlightjs
:revealjs_theme: solarized
:revealjs_transition': linear


Groovy + Google = Groogle

== Presentación

Jorge Aguilera

@jagedn

jorge.aguilera@puravida-software.com

[transition=zoom]
== Qué vamos a ver

[%step]
* DSL
* Groogle

WARNING: Puedo (seguro) estar equivocado en algo (o en todo)
de lo que veremos

== DSL

[quote,Martin Fowler,https://martinfowler.com/books/dsl.html]
DSLs are small languages, focused on a particular aspect of a software system.
You can't build a whole program with a DSL, but you often use multiple DSLs in a
system mainly written in a general purpose language.

=== Ejemplos

.Gradle (Closures)
[source,groovy]
----
repositories {
    mavenCentral()
    jcenter()
    maven {
        url  "https://dl.bintray.com/puravida-software/repo"
    }
}
----

.Spring Integration (Builder)
[source,java]
----
IntegrationFlows.from("example")
    .channel(this.inputChannel())
    .filter((Integer p) -> p > 0)
    .transform(Object::toString)
    .channel(MessageChannels.queue())
    .get();
----

=== Inventado

.Plot
[source,groovy]
----
plot {
    function "cos(sin(x))" and  "x*cos(e*x)" and "t^4/x"
	from (-3) incrementing 0.01 to 3
}
----

http://groovy-lang.gitlab.io/101-scripts/javafx/build_dsl.html

== DSL Groovy

[%step]
- características del propio lenguaje

- Maps

- Closure con @DelegatesTo

=== Propias de Groovy

[source,groovy]
----
// equivalent to: turn(left).then(right)
turn left then right

// equivalent to: take(2.pills).of(chloroquinine).after(6.hours)
take 2.pills of chloroquinine after 6.hours

// equivalent to: paint(wall).with(red, green).and(yellow)
paint wall with red, green and yellow

// with named parameters too
// equivalent to: check(that: margarita).tastes(good)
check that: margarita tastes good
----

=== Maps (de closures)

[source,groovy]
----
def busca(n){
    [ en : { lista->
        for(int i=0; i<lista.length;i++){
            if(lista[i]==n)
                return n
        }
        return null
    }]
}
----

`busca 8 en 1,23,43,12,4/2,8`

=== Closures

[source,groovy]
----
class EmailSpec{
    void from(String from) { println "From: $from"}
    void to(String... to) { println "To: $to"}
    void subject(String subject) { println "Subject: $subject"}
}
class EmailBuilder{
    def email( @DelegatesTo(strategy=Closure.DELEGATE_ONLY,
                            value=EmailSpec) Closure cl) {
        def email = new EmailSpec()
        def code = cl.rehydrate(email, email, email)
        code.resolveStrategy = Closure.DELEGATE_ONLY
        code()
    }
}
----

== Groogle

Groogle es un proyecto abierto que ofrece un
DSL para acceder a servicios de Google

== Google API's

Conjunto de API's que permiten el acceso a servicios de Google
(Gmail, Drive, Sheet, y un largo etcetera)

Casi todas son REST (pero complejas) así que hay librerías Java, Python, etc

https://developers.google.com/api-client-library/java/

== Librería y DSL

- Groogle comenzó como librería para autentificación pero ha evolucionado a
ofrecer DSL específicas en cada servicio, aunque pueden ser usadas en conjunto.

- Uso directo en GroovyScripts

- Groogle está implementado en Groovy pero se puede usar desde
Java 8 mediante _lambdaS_ (no está muy fino)

- Descargar como dependencias desde jCenter

== Motivación

Script para 101-groovy-scripts que accediera al Calendar de un usuario para
hacer un gráfico usando sus eventos.
(https://groovy-lang.gitlab.io/101-scripts/google/calendar.html)

== Casos de Uso

- Compartir información con clientes
- Volcar una tabla a un Sheet compartido (o viceversa)
- Gestionar eventos de un calendario de forma programada
- Servicio REST de una hoja + ficheros en Drive
- etc

== Lenguages/Entornos

- Groovy script

- Java ( min 8)

- Grails, Ratpack

- Dockerizado

== Subproyectos

- Autentificación (OAuth y de servicio) [x]
- Drive [x]
- Sheet [x]
- Calendar [x]

- Chart (en beta y con futuro incierto)  [ ]
- Gmail [ ]
- Team [ ]
- siguiente según demanda [ ]


== Upload a file with Google

[source,java,.stretch]
.DriveSample.java
----
private static File uploadFile(boolean useDirectUpload) throws IOException {
File fileMetadata = new File();
fileMetadata.setTitle(UPLOAD_FILE.getName());

FileContent mediaContent = new
    FileContent("image/jpeg", UPLOAD_FILE);

Drive.Files.Insert insert = drive.files().insert(fileMetadata, mediaContent);
MediaHttpUploader uploader = insert.getMediaHttpUploader();
uploader.setDirectUploadEnabled(useDirectUpload);
uploader.setProgressListener(new FileUploadProgressListener());
return insert.execute();
}
----

==  Upload a file with Groogle

[source,groovy]
----
DriveScript.instance.uploadFile{
    content new File('test.docx')
    saveAs GoogleDoc
}
----

== Autentificación

*groogle-core*

WARNING: Crear una aplicación en la consola de Google junto con unas credenciales.
Necesitamos descargar el _json_ y poder acceder a él (como resource, file, etc)

=== Formas de identificarse

- Autentificación OAuth2: el usuario selecciona la cuenta con la que trabajar

- Cuenta de Servicio. Diferentes usuarios permiten acceder a alguno de sus recursos

=== Tips

Definir los roles que vamos a usar (Drive, Calendar, Sheet ...)

Se crea una carpeta $HOME/.credentials/_${appname}_ donde guardar los tokens

=== Login OAuth

NOTE: Se abre una ventana en un navegador y seleccionamos cuenta de usuario

[source,groovy]
----
include::{groovydir}/LoginDSLTest.groovy[tag=test]
----

=== Login Servicio

[source,groovy]
----
include::{groovydir}/LoginServiceDSLTest.groovy[tag=test]
----

== Drive

- Buscar ficheros/carpetas en el Drive con filtros
- Subir ficheros de nuestro local (y convertirlos automaticamente)
- Bajar ficheros de Drive

=== Buscar ficheros

[source,groovy]
----
DriveScript.instance.with{
    login{
        applicationName 'groogle-example'
        withScopes DriveScopes.DRIVE
        usingCredentials '/groogle-688bcfc07d1b.json'
        asService true
    }
    withFiles {
        nameStartWith 'ABC'
        batchSize 20
        eachFile { file ->
            println """
                $file.id $file.name
                $file.createdDate $file.fileSize ${file.content?.size()}"""
        }
    }
}
----

=== Subir ficheros

[source,groovy]
----
DriveScript.instance.uploadFile{
    name 'hola_caracola.csv'
    mimeType 'text/csv'
    content new ByteArrayInputStream('h1,h2\n100,200'.bytes)
    saveAs GoogleSheet
}
----

== Sheet

- Abrir un SpreadSheet
- Navegar por las filas de un Sheet
- Escribir y leer en un Sheet

=== Leer Sheet

[source,groovy]
----
SheetScript.instance.withSpreadSheet spreadSheetId, { spreadSheet ->
    withSheet 'Hoja 1',{ sheet ->
        def str = A2
        println "A2 vale $str"

        def R2 = readCell("A2")
        println "A2 vale con readCell $R2"

        def A2B2 = readRows("A2", "B2")
        A2B2.each{
            println it
        }
    }
}
----

=== Escribir Sheet

[source,groovy]
----
SheetScript.instance.withSpreadSheet spreadSheetId, { spreadSheet ->
    withSheet 'Hoja 1',{ sheet ->
        A1 = 'Hola'
        B1 = 'Caracola'
    }
}
----


== Calendar

- Navegar por los eventos de un Calendar
- Escribir en un Calendar

=== Nuevo evento

[source,groovy]
----
CalendarScript.instance.createEvent groogleCalendarId, {
    it.event.summary = "quedada"
    allDay new Date().format('yyyy-MM-dd')
}
----

=== Leer eventos

[source,groovy]
----
CalendarScript.instance.withCalendar( groogleCalendarId,{
    batchSize(20)
    eachEvent{ WithEvent withEvent->
        println "Evento $withEvent.event.summary"
    }
})
----

=== Modificar evento

[source,groovy]
----
CalendarScript.instance.withCalendar( groogleCalendarId,{ WithCalendar withCalendar->
    batchSize(20)
    eachEvent{ WithEvent withEvent->
        withEvent.event.summary = "modificado ${new Date()}"
        moveTo new Date().format('yyyy-MM-dd')
    }
})
----

== Demo Time


[%notitle]
== Gracias

image::gracias_multilingue.jpg[background, size=cover]
